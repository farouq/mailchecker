# Mailchecker by Farouk HOUMAIDI
from validate_email import validate_email
import DNS,sys,re,validators

# Setting DNS servers.
DNS.defaults['server']=['8.8.8.8', '8.8.4.4']

# Setting essentials.
toProcess = sys.argv[1] #file to clean
foundEmails 	= 0
goodEmails		= 0

# Create & open results file.
results = open("results.txt", "w")

# Defining checker function
def checkEmail(emailAdress):
	if ( re.match("^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$", emailAdress) != None ) :
		domain = emailAdress.split('@')[1]
		if ( validators.domain(domain) ) :
			if ( validate_email(emailAdress,check_mx=True) == 'True' ) :
				results.write(emailAdress)
				return 'good'

# Iterate through file
with open(toProcess) as file:
	for line in file:
		foundEmails += 1
		if ( checkEmail(line,) == 'good' ) :
			goodEmails += 1


# Close results file
results.close()
print('\n\nEmails processed : ' + str(foundEmails) )
print('Emails found : ' + str(goodEmails) + '\n\n')

exit() 
